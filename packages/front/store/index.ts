export const state = () => ({
  user: false,
  gamesFavourite: false,
  gamesMy: false,
  userOnline: false,
  gamesRecruiting: false,
  news: false,
  postsWeekly: false,
  gamesHot: false,
  forumHot: false,
  quizes: false,
})

export const mutations = {
  setMain(state, main) {
    for(let t in main) {
      state[t] = main[t]
    }
  }
}

export const actions = {
  async nuxtServerInit({ commit }, { app }) {
    const mainPage = await app.$axios.$get('/main');
    commit('setMain', mainPage);
  }
}
