import * as Koa from 'koa';
import { Server } from 'http';
import indexRoutes from './routes';
import middlewares from './middlewares';

const cors = require('@koa/cors');

const app: Koa = new Koa();
const PORT: string | number = process.env.PORT || 10337;

app.use(cors());
app.use(middlewares.logger);
app.use(indexRoutes.routes());

const server: Server = app.listen(PORT, (): void => {
	console.log(`Server listening on port: ${PORT}`);
});

export default server;