import * as Router from 'koa-router';
import { Context } from 'koa';
import mainPage from '../controllers/main';

const router: Router = new Router();

router.get('/main', async (ctx: Context): Promise<void> => {
	const result = await mainPage();
	ctx.status = 200;
	ctx.body = result;
});

export default router;