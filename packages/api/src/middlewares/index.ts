import { Context, Middleware } from "koa";

const logger: Middleware = async (ctx: Context, next: any) => {
    console.log('Url:', ctx.url);
    await next();
};

export default {
    logger,
};