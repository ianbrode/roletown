import { parallel } from 'async';
import axios from 'axios';
const mockedAPi = 'http://0.0.0.0:10338';

export default async function() {
	return new Promise((resolve, reject) => {
		parallel({
			user: (callback) => {
				axios
					.get(mockedAPi + '/user')
					.then(({data}) => {
						callback(null, data);
					});
			},

			userOnline: (callback) => {
				axios
					.get(mockedAPi + '/users/online')
					.then(({data}) => {
						callback(null, data);
					});
			},

			gamesMy: (callback) => {
				axios
					.get(mockedAPi + '/games/my')
					.then(({data}) => {
						callback(null, data);
					});
			},

			gamesFavourite: (callback) => {
				axios
					.get(mockedAPi + '/games/favourite')
					.then(({data}) => {
						callback(null, data);
					});
			},

			gamesRecruiting: (callback) => {
				axios
					.get(mockedAPi + '/games/recruiting')
					.then(({data}) => {
						callback(null, data);
					});
			},

			gamesHot: (callback) => {
				axios
					.get(mockedAPi + '/games/hot')
					.then(({data}) => {
						callback(null, data);
					});
			},

			news: (callback) => {
				axios
					.get(mockedAPi + '/news')
					.then(({data}) => {
						callback(null, data);
					});
			},

			postsWeekly: (callback) => {
				axios
					.get(mockedAPi + '/posts/weekly')
					.then(({data}) => {
						callback(null, data);
					});
			},

			forumHot: (callback) => {
				axios
					.get(mockedAPi + '/forum/hot')
					.then(({data}) => {
						callback(null, data);
					});
			},

			quizes: (callback) => {
				axios
					.get(mockedAPi + '/quizes')
					.then(({data}) => {
						callback(null, data);
					});
			},
		}, function(err, results) {
			console.log(err, results)
			resolve(results);
		})
	});
};;