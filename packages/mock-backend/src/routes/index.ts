import * as Router from 'koa-router';
import { Context } from 'koa';
const faker = require('faker');

const router: Router = new Router();

router.get('/user', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		auth: true,
		roles: ['user'],
		name: faker.internet.userName(),
		id: faker.random.uuid(),
	};
});

router.get('/users/online', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		users: [...Array(100)].map((e, i) => ({
			name: faker.internet.userName(),
			id: faker.random.uuid(),
		})),
	};
});

router.get('/games/my', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		play: [
			{
				name: faker.commerce.productName(),
				id: faker.random.uuid(),
				posts: faker.random.number({min: 0, max: 230}),
				comments: faker.random.number({min: 0, max: 230}),
			}
		],
		lead: [
			{
				name: faker.commerce.productName(),
				id: faker.random.uuid(),
				posts: faker.random.number({min: 0, max: 230}),
				comments: faker.random.number({min: 0, max: 230}),
			}
		],
		read: [
			{
				name: faker.commerce.productName(),
				id: faker.random.uuid(),
				posts: faker.random.number({min: 0, max: 230}),
				comments: faker.random.number({min: 0, max: 230}),
			}
		]
	};
});

router.get('/games/favourite', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		auth: true,
		roles: ['user'],
	};
});

router.get('/games/recruiting', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		auth: true,
		roles: ['user'],
	};
});

router.get('/games/hot', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		auth: true,
		roles: ['user'],
	};
});

router.get('/news', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		auth: true,
		roles: ['user'],
	};
});

router.get('/posts/weekly', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		auth: true,
		roles: ['user'],
	};
});

router.get('/forum/hot', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		auth: true,
		roles: ['user'],
	};
});

router.get('/quizes', (ctx: Context): void => {
	ctx.status = 200;
	ctx.body = {
		auth: true,
		roles: ['user'],
	};
});

export default router;