import * as Koa from 'koa';
import { Server } from "http";
import indexRoutes from './routes';
import middlewares from './middlewares';

const app: Koa = new Koa();
const PORT: string | number = process.env.PORT || 10338;

app.use(middlewares.logger);
app.use(indexRoutes.routes());

const server: Server = app.listen(PORT, (): void => {
	console.log(`Server listening on port: ${PORT}`);
});

export default server;