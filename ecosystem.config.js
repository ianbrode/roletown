const { join } = require('path')

module.exports = {
    apps : [
        {
          name: "roletown-nuxt-front",
          script: "./node_modules/nuxt/bin/nuxt-start",
          cwd: join(__dirname, 'packages', 'front'),
          env: {
              "HOST": "0.0.0.0",
              "PORT": 10339,
              "NODE_ENV": "production",
          }
        },
        {
            name: "roletown-api",
            script: "./packages/api/dist/index.js",
            env: {
                "HOST": "0.0.0.0",
                "PORT": 10337,
                "NODE_ENV": "production",
            }
        },
        {
            name: "roletown-api-fake",
            script: "./packages/mock-backend/dist/index.js",
            env: {
                "HOST": "0.0.0.0",
                "PORT": 10338,
                "NODE_ENV": "production",
            }
        },
    ]
  }